# Bitbucket Pipelines Pipe: cepass/push-docker-image-aws-ecr

Pushing a single docker image to Amazon Elastic container registry.

## Variables
 
| Variable              | Description           |
| --------------------- | ----------------------------------------------------------- |
| AWS_ECR_URL   	| AWS URL to ECR (Without any repository name at then end)	|
| AWS_KEY 			| AWS IAM key |
| AWS_REGION   		| AWS Region	|
| AWS_SECRET     	| AWS IAM secret |
| DOCKERFILE_PATH 	| Path to the directory containing a .dockerfile |
| IMAGE_NAME		| Name of the image saved in AWS ECR |

## YAML Definition (Using repository variables)

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
- pipe: docker://cepass/push-docker-image-aws-ecr
  variables:
    AWS_ECR_URL: $AWS_ECR_URL
    AWS_REGION: $AWS_REGION
    AWS_KEY: $AWS_KEY
    AWS_SECRET: $AWS_SECRET
	DOCKERFILE_PATH: $DOCKERFILE_PATH
	IMAGE_NAME: $IMAGE_NAME
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by pass@certusportautomation.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
